# README #
## Quickstart ##

Import `base.scss`. 

If necessary, customize the default settings overriding the exposed variables.

## Default Settings ##

Variable        | Accepts   | Default  | Description
--------------- | --------- | -------- | -----------
`$__semantic`   | `boolean` | `false`  | Set to `true` to use the placeholder selector in place of the class selector.
`$__ns`         | `string`  | `"dpl-"` | Prefix all the generated rules with a namespace.
`$__ctxPrefix`  | `string`  | `\@`     | Prefix all the generated contexts.
`$__e`          | `string`  | `"-"`   | Prefix an element semantically tied to its block.
`$__m`          | `string`  | `"--"`   | Prefix an element modifier.
`$__fontSize`   | `length`  | `16px`   | Set the base font size for the entire project.
`$__base`       | `length`  | `16px`   | Set the base length used for generating modular scales.
`$__ratio`      | `number`  | `1.5`    | Set the ratio used for generating modular scales.
`$__fraction`   | `(...number)`| `2 3 4 5` | Generate fractional sizes.
`$__steps`      | `number`  | `7` | Max steps count used for stepping rules.
`$__breakpoints`| `(...(string: length))` | See (1) | Used for media queries and contextual rules.
`$__tweakpoints`| `(...(string: length))` | `()` | Used for media queries.

You can customize settings by initializing a variable with the same name before bootstraping the framework.

Ex.
```scss
$__ns: "";
$__fractions: 12;

$__tweakpoints: (
    "broken_navigation": 947px
);

@import "@wearedupl/base/base.scss";
```

### (1) Default Breakpoints ###

Context | Length
------- | ------
`xxs`   | `320px`
`xs`    | `480px`
`s`     | `768px`
`m`     | `992px`
`l`     | `1200px`

## Modularity ##

You can import only the modules you need for your project. 

Importing `base.scss` will import them all.

To customize the build, import the `core.scss` and any modules you may need.

Ex.
```scss
@import 
    "@wearedupl/base/core",
    
    "@wearedupl/base/add/reset",
    "@wearedupl/base/add/layout",
    "@wearedupl/base/add/utils/spacing",
    "@wearedupl/base/add/utils/wrap/responsive";
```

### Modules ###

Imports                                        | Description
---------------------------------------------- | -----------
`@wearedupl/base/base`                         | Bootstrap the framework and imports all modules.
`@wearedupl/base/core`                         | Bootstrap the framework.
`@wearedupl/base/add/reset`                    | Import the `reset` module.
`@wearedupl/base/add/layout`                   | **Row**, **Row--center**, **Row--right**, **Row--middle**, **Row--bottom**, **Row--gap**_$i_, **Row-col**
`@wearedupl/base/add/utils`                    | Manifest for `utils`.
`@wearedupl/base/add/utils/helpers`            | Manifest for `helpers`. Helpers are generic rules that does not fall into a specific category.
`@wearedupl/base/add/utils/helpers/clearfix`   | **cf**
`@wearedupl/base/add/utils/helpers/debug`      | **debug**
`@wearedupl/base/add/utils/offset`             | Manifest for `offset`.
`@wearedupl/base/add/utils/offset/base`        | **push**, **pull**, **push-**_$n_-_$m_, **pull-**_$n_-_$m_
`@wearedupl/base/add/utils/offset/responsive`  | **push**, **pull**, **push-**_$n_-_$m_@_$ctx_, **pull-**_$n_-_$m_@_$ctx_
`@wearedupl/base/add/utils/size`               | Manifest for `size`.
`@wearedupl/base/add/utils/size/base`          | **size-**_$n_-_$m_
`@wearedupl/base/add/utils/size/responsive`    | **size-**_$n_-_$m_@_$ctx_
`@wearedupl/base/add/utils/spacing`            | Manifest for `spacing`.
`@wearedupl/base/add/utils/spacing/base`       | **m**_$i_, **mh**_$i_, **mv**_$i_, **mt**_$i_, **mb**_$i_, **ml**_$i_, **mr**_$i_, **nmt**_$i_, **nmb**_$i_, **nml**_$i_, **nmr**_$i_, **ml-**_$n_-_$m_, **mr-**_$n_-_$m_, **nml-**_$n_-_$m_, **nmr-**_$n_-_$m_, **p**_$i_, **ph**_$i_, **pv**_$i_, **pt**_$i_, **pb**_$i_, **pl**_$i_, **pr**_$i_
`@wearedupl/base/add/utils/spacing/responsive` | **m**_$i_@_$ctx_, **mh**_$i_@_$ctx_, **mv**_$i_@_$ctx_, **mt**_$i_@_$ctx_, **mb**_$i_@_$ctx_, **ml**_$i_@_$ctx_, **mr**_$i_@_$ctx_, **nmt**_$i_@_$ctx_, **nmb**_$i_@_$ctx_, **nml**_$i_@_$ctx_, **nmr-**_$i_@_$ctx_, **ml-**_$n_-_$m_@_$ctx_, **mr-**_$n_-_$m_@_$ctx_, **nml-**_$n_-_$m_@_$ctx_, **nmr-**_$n_-_$m_@_$ctx_, **p**_$i_@_$ctx_, **ph**_$i_@_$ctx_, **pv**_$i_@_$ctx_, **pt**_$i_@_$ctx_, **pb**_$i_@_$ctx_, **pl**_$i_@_$ctx_, **pr**_$i_@_$ctx_
`@wearedupl/base/add/utils/wrap`               | Manifest for `wrap`.
`@wearedupl/base/add/utils/wrap/base`          | **wrap**, **wrap--**_$ctx_
`@wearedupl/base/add/utils/wrap/responsive`    | **wrap**, **wrap--**_$ctx_@_$ctx_

Where

* `$n-$m` is a fraction as `3` of `5` _(from `$__fractions`)_
* `$ctx` is the context _(from `$__breakpoints`)_
* `$i` is a step _(from `$__steps`)_

## Example ##

```html 
<div class="dpl-wrap dpl-wrap--l@l dpl-wrap--m@m dpl-wrap--s@s dpl-wrap--xs@xs  dpl-debug">
        
        <div class="dpl-Row dpl-Row--gap3  dpl-mb3 dpl-mb4@xs">
            <div class="dpl-Row-col  dpl-size-1-2@s dpl-size-3-5@m dpl-size-4-5@l">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis repudiandae, illum. Corrupti iusto consectetur reprehenderit modi, deserunt? Dolor, voluptatibus nostrum tempore perspiciatis exercitationem atque esse adipisci amet, non, quasi laborum.</div>
            <div class="dpl-Row-col  dpl-size-1-2@s dpl-size-2-5@m dpl-size-1-5@l">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur similique officia numquam quibusdam, vero beatae. Rem in odio asperiores illum incidunt distinctio quasi, repellat illo perferendis quam eaque, vel voluptate.</div>
        </div>

        <div class="dpl-Row dpl-Row--gap3">
            <div class="dpl-Row-col  dpl-size-1-3@xs  dpl-push-2-3@xs  dpl-mb3 dpl-mb0@xs">1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque placeat vero voluptas consequatur debitis, similique beatae laborum! Tenetur, quibusdam, ullam? Ea, eum, laudantium. Quis enim adipisci voluptatem, dolore a deleniti!</div>
            <div class="dpl-Row-col  dpl-size-1-3@xs  dpl-pull-1-3@xs">2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eligendi, facere eveniet mollitia. Deserunt, unde, illo. Beatae porro officia, eos. Modi sit adipisci asperiores nihil cum accusantium necessitatibus! Iste, officiis.</div>
            <div class="dpl-Row-col  dpl-size-1-3@xs  dpl-pull-1-3@xs">3. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error veritatis voluptas inventore magnam. Amet blanditiis eius, aspernatur dolorem magnam, a neque, molestiae cum quae porro unde est inventore recusandae architecto.</div>
        </div>
    </div>
```